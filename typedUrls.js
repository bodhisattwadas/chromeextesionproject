function onAnchorClick(event) {
  chrome.tabs.create({
    selected: true,
    url: event.srcElement.href
  });
  return false;
}
function buildPopupDom(divName, data) {
  var popupDiv = document.getElementById(divName);
  var ul = document.createElement('ul');
  popupDiv.appendChild(ul);
  for (var i = 0, ie = data.length; i < ie; ++i) {
    var a = document.createElement('a');
    a.href = data[i];
    console.log(data[i]);
    a.appendChild(document.createTextNode(data[i]));
    a.addEventListener('click', onAnchorClick);
    var li = document.createElement('li');
    li.appendChild(a);
    ul.appendChild(li);
  }
}

function buildTypedUrlList(divName) {
  var microsecondsPerWeek = 1000 * 60 * 60  ;
  var oneHourAgo = (new Date).getTime() - microsecondsPerWeek;
  var numRequestsOutstanding = 0;

  chrome.history.search({
      'text': '',              // Return every history item....
      'startTime': oneHourAgo  // that was accessed less than one week ago.
    },
    function(historyItems) {
      // For each history item, get details on all visits.
      for (var i = 0; i < historyItems.length; ++i) {
        var url = historyItems[i].url;
        console.log(historyItems[i].lastVisitTime);
        var processVisitsWithUrl = function(url) {
          return function(visitItems) {
            processVisits(url, visitItems);
          };
        };
        chrome.history.getVisits({url: url}, processVisitsWithUrl(url));
        numRequestsOutstanding++;
      }
      if (!numRequestsOutstanding) {
        onAllVisitsProcessed();
      }
    });
  var urlToCount = {};
  var processVisits = function(url, visitItems) {
    for (var i = 0, ie = visitItems.length; i < ie; ++i) {
      /**
       * Comment this block if you want to get all URL list instead of links only
       */  
      if (visitItems[i].transition != 'link') {
        continue;
      }
      /**
       * Comment this block if you want to get all URL list instead of links only
       */
      if (!urlToCount[url]) {
        urlToCount[url] = 0;
      }
      urlToCount[url]++;
    }
    if (!--numRequestsOutstanding) {
      onAllVisitsProcessed();
    }
  };
  var onAllVisitsProcessed = function() {
    urlArray = [];
    for (var url in urlToCount) {
      urlArray.push(url);
    }
    urlArray.sort(function(a, b) {
      return urlToCount[b] - urlToCount[a];
    });
    buildPopupDom(divName, urlArray);
  };
}
document.addEventListener('DOMContentLoaded', function () {
  buildTypedUrlList("typedUrl_div");
});